import { useEffect } from 'react'

// Custom hook to set document title
const useDocumentTitle = (title: string) => {
  useEffect(() => {
    document.title = `${title} | KhánhĐặng`
  }, [title])
}

export default useDocumentTitle
