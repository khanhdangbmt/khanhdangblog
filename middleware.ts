import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'
import { createClient } from 'libs/supabase/server'
import { toast } from './components/hooks/use-toast'

export async function middleware(req: NextRequest) {
  const supabase = createClient()
  const user = await (await (await supabase).auth.getSession()).data.session?.user

  if (!user || user.email !== process.env.NEXT_ADMIN_EMAIL) {
    toast({
      title: "You're not admin ;)",
      description: 'Contact to me to access more',
    })
    return NextResponse.redirect(new URL('/', req.url))
  }

  return NextResponse.next()
}

export const config = {
  matcher: ['/private/:path*'],
}
