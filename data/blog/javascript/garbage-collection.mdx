---
title: 'Garbage collection'
date: '2024-01-16'
lastmod: '2024-02-02'
tags: ['javascript']
draft: false
layout: PostLayout
summary: 'The mechanism of garbage collection in javascript.'
authors: ['default']
---

# Garbage collection

Memory management in JavaScript is handled automatically by the engine. This article describes the mechanism of handling unused memory areas.

# Reachability

The main concept in memory management in JavaScript is ***reachability***.

1. Reachability values refer to the ability to access or use something in some way. There are a set of rules that specify that accessible values cannot be deleted:

- The currently executing function, its local variables, and parameters.
- Other functions on the current chain of nested calls, their local variables, and parameters.
- Global variables.
- (there are some other, internal ones as well)

 → These values are called ***roots***.
    
2. Other values are considered reachable if they are accessible from the roots through references or a chain of references.

The garbage collector monitors all objects and deletes those that have become unreachable.

# A simple example of reachability:
```javascript
// user has a reference to the object
let user = {
  name: "John"
};
```
<p align="center">
 <img src="/static/images/blog/js/garbage-collection.png" width={200}/>
</p>


This is the value of the global variable user referencing an object ```{name: "John"}```. If the value of user is changed to null, the reference is lost.
```js
user = null;
```
<p align="center">
 <img src="/static/images/blog/js/garbage-collection-1.png" width={200}/>
</p>

he object ```{name: "John"}``` becomes unreachable because there are no references to it. The garbage collector will clean up and free the memory.

# Two references
Now we have two references to the same object:

```javascript
// user has a reference to the object
let user = {
  name: "John"
};

let admin = user;
```
<p align="center">
 <img src="/static/images/blog/js/garbage-collection-2.png" width={200}/>
</p>

And set 
```
user = null;
```

→ The object is not deleted because admin still references the object. If the admin variable is also overwritten, the object will be deleted.

# Interlinked objects
In the case where an object is linked to another object but it itself has no references, it will still be deleted.
```javascript
function marry(man, woman) {
  woman.husband = man;
  man.wife = woman;

  return {
    father: man,
    mother: woman
  }
}

let family = marry({
  name: "John"
}, {
  name: "Ann"
});
```
<p align="center">
 <img src="/static/images/blog/js/garbage-collection-3.png" width={400}/>
</p>

Delete the reference to ```{name:”John”}```
```javascript
delete family.father;
delete family.mother.husband;
```

If one of the two references is deleted, it still does not meet the condition to delete ```{name: "John"}```.

<p align="center">
 <img src="/static/images/blog/js/garbage-collection-4.png" width={400}/>
</p>

However, if both references are deleted, ```{name: "John"}``` will no longer have any references to it.

<p align="center">
 <img src="/static/images/blog/js/garbage-collection-5.png" width={400}/>
</p>

References from ```{name: "John"}``` to other objects are not relevant, so ```{name: "John"}``` is no longer reachable. The garbage collector will delete it and reclaim the memory.

<p align="center">
 <img src="/static/images/blog/js/garbage-collection-6.png" width={400}/>
</p>

# Unreachable island
The entire object of the linked object will become unreachable and will be deleted and memory will be reclaimed.
```javascript
family = null;
```
<p align="center">
 <img src="/static/images/blog/js/garbage-collection-7.png" width={400}/>
</p>

# References
- <a href="https://javascript.info/garbage-collection" className="no-underline">Garbage collection</a>
- <a href="https://jayconrod.com/posts/55/a-tour-of-v8-garbage-collection" className="no-underline">A tour of V8</a>
- <a href="https://v8.dev/" className="no-underline">V8 Engine</a>
