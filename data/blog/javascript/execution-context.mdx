---
title: 'Execution Context'
date: '2024-01-16'
lastmod: '2024-02-02'
tags: ['javascript']
draft: false
layout: PostLayout
summary: 'Execution context is the environment where code is executed, consisting of three main contexts...'
authors: ['default']
---
# Introduction
Execution context is the environment where code is executed, consisting of three main contexts:

- Global code: the default environment where code runs initially.
- Functional code: the environment within a function() when it is executed.

# Phases of Execution context
1. Creation phase: The JavaScript engine creates an execution context and sets up its environment. It initializes variables, functions, and the scope chain for the execution context.

2. Execution phase: The JavaScript engine executes the code within the execution context. It runs statements, evaluates expressions in the script, and executes any invoked functions.

Everything in JavaScript happens within an execution context, which is divided into two parts: ***Memory*** and ***Code***. Importantly, all components and phases apply to both global and functional contexts.

## Creation phase

<p align="center">
 <img src="/static/images/blog/js/execution-context-creation-phase.png" width={400}/>
</p>

### Example
```javascript
var n = 5;

function square(n) {
  var ans = n * n;
  return ans;
}

var square1 = square(n);
var square2 = square(8);  

console.log(square1)
console.log(square2)
```

Right from the start, the JavaScript engine executes the entire code and creates the global execution context, performing tasks such as:

- Creating the global object like window in the browser or global in Node.js.
- Setting up memory to store variables and functions.
- Storing variables with default values or undefined, and functions as references.

This step is the creation phase, which can be depicted as follows:
<p align="center">
 <img src="/static/images/blog/js/excution-context-creation-phase-example.png" width={400}/>
</p>
After the creation phase, the next step is to move to the code execution phase.

# Execution phase
During this phase, the JavaScript engine executes each line of code from top to bottom. The value of n in the code will be assigned as 5 - initially, its default value after the creation phase was undefined.

When it reaches the line where the square function is defined - a function that has already been allocated memory, it directly jumps to the line ```var square1 = square(n)```, and the ```square()``` function is called. The JavaScript engine will initialize this function once again.

<p align="center">
 <img src="/static/images/blog/js/excution-context-execute-phase.png" width={400}/>
</p>

After the function executes and its return value is assigned to the variable square1, the function execution context will be destroyed. Once all the code has been executed, the execution context will appear as shown in the diagram below, and it will also be destroyed.

<p align="center">
 <img src="/static/images/blog/js/excution-context-execute-phase-example.png" width={400}/>
</p>

# Call Stack
To track all contexts, the JavaScript Engine uses a Call Stack, also known as the ***Execution Context Stack***, Runtime Stack, or Machine Stack.

The Call Stack operates on a Last In, First Out (LIFO) basis.

Example of a call stack with the following code snippet:
```js
function funcA(m,n) {
    return m * n;
}

function funcB(m,n) {
    return funcA(m,n);
}

function getResult(num1, num2) {
    return funcB(num1, num2)
}

var res = getResult(5,6);

console.log(res); // 30
```
Callstack look like this:
<p align="center">
 <img src="/static/images/blog/js/execution-context-call-stack.png" />
</p>
If the call stack reaches its limit, it will receive an error message like this:
```javascript
function display() {
    display();
}

display();
```

Error:
```javascript
C:\Users\rwiteshbera\Desktop\Javascript\n.js:2
    display();
    ^
RangeError: Maximum call stack size exceeded
```

# References
- <a href="https://www.freecodecamp.org/news/how-javascript-works-behind-the-scene-javascript-execution-context/" className="no-underline">Javascript execution context</a>
