interface Project {
  title: string
  description: string
  href?: string
  imgSrc?: string
}

const projectsData: Project[] = [
  {
    title: 'Todo list',
    description: `Simply todo list with nextjs, tailwindcss, nestjs.`,
    imgSrc: '',
    href: 'https://vercel.com/khanhs-projects-81836ffa/todo-demo',
  },
  {
    title: 'Show working days',
    description: `Show working days data with auto load new data when scroll to bottom.`,
    imgSrc: '',
    href: 'https://show-data-working-days.vercel.app',
  },
  {
    title: 'Khanhdang blogs',
    description: `My Profile.`,
    imgSrc: '',
    href: 'https://khanhdang.name',
  },

  {
    title: 'Kdcoin',
    description: `My personal coin - demo the mechanism of cryptocurrency.`,
    imgSrc: '',
    href: ' https://github.com/KHANHDANGBMT/gooup1-nestjs-template',
  },

  {
    title: 'Content management api',
    description: `Upload and retrieve content data - nestjs.`,
    imgSrc: '',
    href: 'https://gitlab.com/khanhdangbmt/content-management-api',
  },

  // {
  //   title: 'Bensist',
  //   description: `Building the benefit application for the employee. Any company can integrate with the application and sign a contract for their employee benefit instead of giving the money to them. That will be advantageous to their employees more than money.`,
  //   imgSrc: '/static/images/projects/bensist/image.png',
  //   href: 'https://app.bensist.com',
  // },
]

export default projectsData
