const headerNavLinks = [
  { href: '/', title: 'Home' },
  { href: '/tools', title: 'Tools' },
  { href: '/blog', title: 'Blog' },
  // { href: '/tags', title: 'Tags' },
  { href: '/projects', title: 'Projects' },
  { href: '/about', title: 'About' },
]

export default headerNavLinks
