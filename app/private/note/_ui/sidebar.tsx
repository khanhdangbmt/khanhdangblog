import clsx from 'clsx'
import { useNoteStore } from 'store/noteStore'

export default function Sidebar() {
  const { notes, addNote, deleteNote, selectNote, selectedNoteId } = useNoteStore()

  return (
    <div className="h-screen w-1/4 border-r bg-gray-100 p-4">
      <button className="w-full rounded bg-blue-500 p-2 text-white" onClick={addNote}>
        + New Note
      </button>
      <ul className="mt-4">
        {notes.map((note) => (
          <li
            key={note.id}
            className={clsx('flex cursor-pointer items-center justify-between border-b p-2', {
              'bg-gray-300': selectedNoteId === note.id,
            })}
            onClick={() => selectNote(note.id)}
          >
            <span className="truncate">{note.title || 'Untitled'}</span>
            <button
              className="text-red-500"
              onClick={(e) => {
                e.stopPropagation()
                deleteNote(note.id)
              }}
            >
              🗑
            </button>
          </li>
        ))}
      </ul>
    </div>
  )
}
