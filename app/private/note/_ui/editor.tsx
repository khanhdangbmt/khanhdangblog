import { useNoteStore } from 'store/noteStore'

export default function Editor() {
  const { notes, selectedNoteId, updateNote } = useNoteStore()
  const note = notes.find((n) => n.id === selectedNoteId)

  if (!note) return <div className="p-4 text-gray-500">Select or create a note</div>

  return (
    <textarea
      className="h-screen w-full resize-none p-4 text-lg focus:outline-none"
      value={note.content}
      onChange={(e) => updateNote(note.id, e.target.value)}
      placeholder="Start typing..."
    />
  )
}
