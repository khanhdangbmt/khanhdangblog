'use client'
import Editor from './_ui/editor'
import Sidebar from './_ui/sidebar'

const NotePage = () => {
  return (
    <div className="flex">
      <Sidebar />
      <div className="w-3/4">
        <Editor />
      </div>
    </div>
  )
}

export default NotePage
