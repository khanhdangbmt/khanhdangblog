'use client'
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '@/components/atoms/dialog'
import { Form, FormControl, FormField, FormItem, FormMessage } from '@/components/atoms/form'
import { Input } from '@/components/atoms/input'
import clsx from 'clsx'
import { createClient } from 'libs/supabase/client'
import { Check, EditIcon, Plus, TrashIcon } from 'lucide-react'
import { useEffect, useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { z } from 'zod'
import Button from '@/components/atoms/button'
import { Badge } from '@/components/atoms/badge'
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/atoms/select'
import { Textarea } from '@/components/atoms/textarea'

interface ITodo {
  id: number
  created_at: string
  owner_id: string
  title: string
  description: string
  is_done: boolean
  due_date: string
  group_id: number
  groups: IGroup
}

interface IGroup {
  id: number
  created_at: string
  name: string
  type: string
}

const FormSchema = z.object({
  name: z.string().min(2, {
    message: 'name must be at least 2 characters.',
  }),
})

const TodoPage = () => {
  const [todos, setTodos] = useState<ITodo[]>([])
  const [groups, setGroups] = useState<IGroup[]>([])
  const [editingTodo, setEditingTodo] = useState<ITodo | null>(null)
  const [filterTodo, setFilterTodo] = useState<'all' | 'checked' | 'unchecked'>('all')
  const supabaseClient = createClient()
  const [selectedGroup, setSelectedGroup] = useState<number>(1)
  const todoRef = useRef<HTMLInputElement>(null)
  const todoDesRef = useRef<HTMLTextAreaElement>(null)
  const [openGroupModal, setOpenGroupModal] = useState(false)
  const [filterGroup, setFilterGroup] = useState<number>(0)

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      name: '',
    },
  })

  const fetchTodos = async () => {
    console.log('fetching todos... with group: ', filterGroup)
    let query = supabaseClient
      .from('todos')
      .select('*, groups(name, id)')
      .order('created_at', { ascending: false })

    if (filterTodo !== 'all') {
      if (filterTodo === 'checked') {
        query = query.eq('is_done', true)
      }
      if (filterTodo === 'unchecked') {
        query = query.eq('is_done', false)
      }
    }

    if (filterGroup !== 0 && Number.isNaN(filterGroup) === false) {
      query = query.eq('group_id', filterGroup)
    }

    const { data, error } = await query
    if (error) {
      console.error('error', error)
    } else {
      setTodos(data)
    }
  }

  const fetchGroups = async () => {
    const { data, error } = await supabaseClient.from('groups').select('*')
    if (error) {
      console.error('group error', error)
    } else {
      console.log('group data', data)
      setGroups(data)
    }
  }

  useEffect(() => {
    console.log('calling to fetch todo again...')
    fetchTodos()
  }, [filterTodo, filterGroup])

  useEffect(() => {
    fetchGroups()
  }, [selectedGroup])

  const createTodo = async ({ title }: { title: string }) => {
    const { data, error } = await supabaseClient.from('todos').insert([
      {
        title: title,
        group_id: selectedGroup,
        description: todoDesRef.current?.value ?? '',
      },
    ])

    if (error) {
      console.error('error', error)
    } else {
      console.log('data', data)
      fetchTodos()
      todoDesRef.current!.value = ''
    }
  }

  const deleteTodo = async (id: number) => {
    const { data, error } = await supabaseClient.from('todos').delete().eq('id', id)
    if (error) {
      console.error('error', error)
    } else {
      console.log('data', data)
      fetchTodos()
    }
  }

  const onSubmit = async ({ name }: z.infer<typeof FormSchema>) => {
    const { data, error } = await supabaseClient.from('groups').insert([{ name }])

    if (error) {
      console.error('error', error)
    } else {
      fetchGroups()
      setOpenGroupModal(false)
    }
  }

  return (
    <div className="flex flex-col items-center justify-center">
      <div className="max-w-[900px]">
        <Input
          ref={todoRef}
          placeholder="What do you want to do next?"
          className="min-h-[50px] w-full min-w-[300px] p-4 text-lg md:min-w-[500px]"
          onKeyDown={(value) => {
            if (value.key === 'Enter') {
              createTodo({ title: value.currentTarget.value }).then(() => {
                value.currentTarget.value = ''
              })
              value.currentTarget.value = ''
            }
          }}
        />
        <Textarea
          name="description"
          id="todo-des"
          className="mt-2"
          placeholder="Todo description..."
          ref={todoDesRef}
        ></Textarea>
        <div className="mt-2 flex items-center justify-between">
          <div className="flex gap-2">
            <Select onValueChange={(value) => setSelectedGroup(parseInt(value))}>
              <SelectTrigger>
                <SelectValue placeholder="General" />
              </SelectTrigger>
              <SelectContent>
                {groups.map((group) => {
                  return (
                    <SelectItem className="cursor-pointer" key={group.id} value={group.id + ''}>
                      {group.name}
                    </SelectItem>
                  )
                })}
              </SelectContent>
            </Select>
            <Dialog open={openGroupModal} onOpenChange={setOpenGroupModal}>
              <DialogTrigger>
                <Plus />
              </DialogTrigger>
              <DialogContent>
                <DialogHeader>
                  <DialogTitle>Create new group</DialogTitle>
                  <Form {...form}>
                    <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-6">
                      <FormField
                        control={form.control}
                        name="name"
                        render={({ field }) => (
                          <FormItem>
                            <FormControl>
                              <Input placeholder="Group of todo" {...field} />
                            </FormControl>
                            <FormMessage />
                          </FormItem>
                        )}
                      />
                      <div className="flex w-full justify-end">
                        <Button type="submit" variant="sm">
                          Submit
                        </Button>
                      </div>
                    </form>
                  </Form>
                </DialogHeader>
              </DialogContent>
            </Dialog>
          </div>
          <Button
            variant="sm"
            onClick={() => {
              createTodo({
                title: todoRef.current?.value ?? '',
              }).then(() => {
                todoRef.current!.value = ''
              })
            }}
          >
            Create Todo
          </Button>
        </div>
      </div>

      <div className="relative mt-4 w-full p-2 md:w-[700px] md:p-0">
        {todos.length === 0 ? (
          <div className="text-center text-gray-500">No todos available</div>
        ) : (
          todos.map((todo) => (
            <div key={todo.id} className="flex flex-col gap-1 border-b p-2">
              <div className="flex items-center">
                <input
                  id={'todo-checkbox-' + todo.id}
                  type="checkbox"
                  checked={todo.is_done}
                  onChange={async () => {
                    const { error } = await supabaseClient
                      .from('todos')
                      .update({ is_done: !todo.is_done })
                      .eq('id', todo.id)
                    if (error) {
                      console.error('error', error)
                    } else {
                      fetchTodos()
                    }
                  }}
                  className="mr-2"
                  style={{ fontSize: '20px' }}
                />
                <div className="flex w-full flex-col justify-between gap-2">
                  <div className="flex flex-row justify-between">
                    <div className="flex-1 break-all">
                      {editingTodo && editingTodo.id === todo.id ? (
                        <div className="w-ful flex items-center gap-2">
                          <input
                            className="w-full"
                            type="text"
                            defaultValue={todo.title}
                            onBlur={(e) => {
                              const updatedTitle = e.currentTarget.value
                              supabaseClient
                                .from('todos')
                                .update({ title: updatedTitle })
                                .eq('id', todo.id)
                                .then(({ data, error }) => {
                                  if (error) {
                                    console.error('error', error)
                                  } else {
                                    setTodos((prevTodos) =>
                                      prevTodos.map((t) =>
                                        t.id === todo.id ? { ...t, title: updatedTitle } : t
                                      )
                                    )
                                    setEditingTodo(null)
                                  }
                                })
                            }}
                          />
                          <button onClick={() => setEditingTodo(null)} className="w-fit">
                            <Check />
                          </button>
                        </div>
                      ) : (
                        <h3
                          className={clsx(
                            'text-xl font-semibold',
                            todo.is_done && 'italic line-through'
                          )}
                          aria-label={'todo-checkbox-' + todo.id}
                        >
                          {todo.title}
                        </h3>
                      )}
                      <p className="text-gray-700 dark:text-gray-400">{todo.description}</p>
                      {!(editingTodo && editingTodo.id === todo.id) && (
                        <p className="w-fit whitespace-nowrap text-xs italic text-[#545d7e]">
                          {new Date(todo.due_date).toLocaleString('en-US', {
                            weekday: 'short',
                            year: 'numeric',
                            month: 'short',
                            day: 'numeric',
                          })}
                        </p>
                      )}
                    </div>

                    <div className="flex justify-center gap-2">
                      <button
                        onClick={() => deleteTodo(todo.id)}
                        className="text-red-500 hover:text-red-700"
                      >
                        <TrashIcon size={16} />
                      </button>
                      <button
                        onClick={() => setEditingTodo(todo)}
                        className="text-blue-500 hover:text-blue-700"
                      >
                        <EditIcon size={16} />
                      </button>
                    </div>
                  </div>

                  <div>
                    <Badge variant="outline">{todo.groups.name}</Badge>
                  </div>
                </div>
              </div>
            </div>
          ))
        )}

        <div className="sticky bottom-0 flex flex-col justify-between gap-2 bg-white p-2 dark:bg-black md:flex-row">
          <div className="flex gap-4">
            <label className="flex cursor-pointer items-center gap-2">
              <input
                type="radio"
                name="filter"
                value="all"
                defaultChecked
                onChange={() => setFilterTodo('all')}
              />
              All
            </label>
            <label className="flex cursor-pointer items-center gap-2">
              <input
                type="radio"
                name="filter"
                value="unchecked"
                onChange={() => setFilterTodo('unchecked')}
              />
              Unchecked
            </label>
            <label className="flex cursor-pointer items-center gap-2">
              <input
                type="radio"
                name="filter"
                value="checked"
                onChange={() => setFilterTodo('checked')}
              />
              Checked
            </label>
          </div>

          <div className="w-fit">
            <Select onValueChange={(value) => setFilterGroup(parseInt(value))}>
              <SelectTrigger>
                <SelectValue placeholder="All" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem className="cursor-pointer" key="all" value="all">
                  All
                </SelectItem>
                {groups.map((group) => {
                  return (
                    <SelectItem className="cursor-pointer" key={group.id} value={group.id + ''}>
                      {group.name}
                    </SelectItem>
                  )
                })}
              </SelectContent>
            </Select>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TodoPage
