import Link from 'next/link'

const Card = ({ children }) => {
  return (
    <div className="group max-w-[300px] cursor-pointer rounded border border-gray-300 p-4 hover:bg-primary-500 hover:text-white">
      {children}
    </div>
  )
}

const PrivatePage = () => {
  return (
    <div className='flex gap-2'>
      <Link href={'/private/todo'}>
        <Card>
          <h2 className='font-semibold'>Todo List</h2>
          <p className="text-sm text-gray-500 group-hover:text-white">All of my works</p>
        </Card>
      </Link>
      <Link href={'/private/note'}>
        <Card>
          <h2 className='font-semibold'>Note</h2>
          <p className="text-sm text-gray-500 group-hover:text-white">Note everything</p>
        </Card>
      </Link>
    </div>
  )
}

export default PrivatePage
