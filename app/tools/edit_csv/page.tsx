'use client'
import Button from '@/components/atoms/button'
import useDocumentTitle from 'hooks/useDocumentTitle'
import { useEffect, useState } from 'react'
import { read, utils, writeFile } from 'xlsx'
import { Trash2 } from 'lucide-react'

const EditCsvPage = () => {
  const [data, setData] = useState<string[][]>([])
  const [headers, setHeaders] = useState<string[]>([])

  useEffect(() => {
    const cachedData = localStorage.getItem('csvData')
    const cachedHeaders = localStorage.getItem('csvHeaders')
    if (cachedData && cachedHeaders) {
      setData(JSON.parse(cachedData))
      setHeaders(JSON.parse(cachedHeaders))
    }
    return () => {
      localStorage.setItem('csvData', JSON.stringify(data))
      localStorage.setItem('csvHeaders', JSON.stringify(headers))
    }
  }, [])

  useDocumentTitle('Edit CSV')

  const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0]
    if (file) {
      const reader = new FileReader()
      reader.onload = (e) => {
        const binaryStr = e.target?.result
        const workbook = read(binaryStr, { type: 'binary' })
        const sheetName = workbook.SheetNames[0]
        const sheet = workbook.Sheets[sheetName]
        const jsonData = utils.sheet_to_json<string[]>(sheet, { header: 1, defval: '' })
        setHeaders(jsonData[0] as string[])
        setData(jsonData.slice(1) as string[][])
      }
      reader.readAsBinaryString(file)
    }
  }

  const handleCellChange = (rowIndex: number, colIndex: number, value: string) => {
    const newData = [...data]
    newData[rowIndex][colIndex] = value
    setData(newData)
  }

  const handleHeaderChange = (colIndex: number, value: string) => {
    const newHeaders = [...headers]
    newHeaders[colIndex] = value
    setHeaders(newHeaders)
  }

  const handleExport = () => {
    const worksheet = utils.aoa_to_sheet([headers, ...data])
    const workbook = utils.book_new()
    utils.book_append_sheet(workbook, worksheet, 'Sheet1')
    writeFile(workbook, new Date().toISOString().split('T')[0] + '__Khanh_Dang.csv')
  }

  const addColumn = (position: number) => {
    const newData = data.map((row) => {
      const newRow = [...row]
      newRow.splice(position, 0, '')
      return newRow
    })
    setData(newData)
    const newHeaders = [...headers]
    newHeaders.splice(position, 0, '')
    setHeaders(newHeaders)
  }

  const addRow = (position: number) => {
    const newRow = headers.map(() => '')
    const newData = [...data]
    newData.splice(position, 0, newRow)
    setData(newData)
  }

  const deleteRow = (position: number) => {
    const newData = data.filter((_, index) => index !== position)
    setData(newData)
  }

  return (
    <div className="overflow-hidden">
      <h1 className="mb-4 text-2xl font-bold">Edit CSV File</h1>
      <div className="mb-4 flex flex-col items-center justify-between md:flex-row">
        <input type="file" accept=".csv" onChange={handleFileUpload} />
        <div className="mt-4 flex flex-wrap gap-2 md:mt-0 md:flex-auto">
          <Button
            variant="sm"
            onClick={() => {
              const position = parseInt(
                prompt('Enter the position to add a new column:') ?? '0',
                10
              )
              if (!isNaN(position) && position >= 0 && position <= headers.length) {
                addColumn(position)
              } else {
                alert('Invalid position')
              }
            }}
          >
            Add new column
          </Button>
          <Button
            variant="sm"
            onClick={() => {
              const position = parseInt(prompt('Enter the position to add a new row:') ?? '0', 10)
              if (!isNaN(position) && position >= 0 && position <= data.length) {
                addRow(position)
              } else {
                alert('Invalid position')
              }
            }}
          >
            Add new row
          </Button>
          <Button variant="sm" onClick={handleExport}>
            Export to CSV
          </Button>
        </div>
      </div>
      {headers.length > 0 && (
        <div className="w-full overflow-auto">
          <table className="w-full">
            <thead>
              <tr>
                <th className="border border-gray-300 bg-white p-2"></th>
                <th className="border border-gray-300 bg-white p-2">Index</th>
                {headers.map((header, colIndex) => (
                  <th key={colIndex}>
                    <input
                      type="text"
                      value={header}
                      onChange={(e) => handleHeaderChange(colIndex, e.target.value)}
                      className="border border-gray-300 dark:text-black"
                    />
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {data.map((row, rowIndex) => (
                <tr key={rowIndex} className="hover:bg-gray-400 dark:text-black">
                  <td className="border border-gray-300 bg-white p-2">
                    <Button variant="sm" onClick={() => deleteRow(rowIndex)}>
                      <Trash2 size={16} />
                    </Button>
                  </td>
                  <td className="border border-gray-300 bg-white p-2">{rowIndex}</td>
                  {row.map((cell, colIndex) => (
                    <td key={colIndex}>
                      <input
                        type="text"
                        value={cell}
                        onChange={(e) => handleCellChange(rowIndex, colIndex, e.target.value)}
                        className="border border-gray-300"
                      />
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  )
}

export default EditCsvPage
