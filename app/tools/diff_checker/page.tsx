'use client'
import * as Diff from 'diff'
import React, { useState, Suspense } from 'react'
import Button from '@/components/atoms/button'
import useDocumentTitle from 'hooks/useDocumentTitle'

const DiffCheckerPage: React.FC = () => {
  const [text1, setText1] = useState('')
  const [text2, setText2] = useState('')
  const [diffResult, setDiffResult] = useState<JSX.Element[]>([])

  useDocumentTitle('Diff Checker')

  const handleCompare = (type: 'chars' | 'words' | 'lines') => {
    let diff
    switch (type) {
      case 'chars':
        diff = Diff.diffChars(text1, text2)
        break
      case 'words':
        diff = Diff.diffWords(text1, text2)
        break
      case 'lines':
        diff = Diff.diffLines(text1, text2)
        break
      default:
        diff = []
    }
    setDiffResult(
      diff.map((part) => {
        let color = 'grey'
        if (part.added) {
          color = 'green'
        } else if (part.removed) {
          color = 'red'
        }
        return (
          <span style={{ color }} key={Math.random()}>
            {part.value}
          </span>
        )
      })
    )
  }

  return (
    <Suspense fallback="loading...">
      <div className="primary-scrollbar relative flex min-h-1 flex-1 flex-col overflow-auto">
        <div className="sticky top-0 flex flex-col">
          <h1 className="text-2xl font-bold">Diff Checker</h1>
          <div className="flex gap-2 py-2">
            <Button variant="sm" onClick={() => handleCompare('chars')}>
              Compare by Chars
            </Button>
            <Button variant="sm" onClick={() => handleCompare('words')}>
              Compare by Words
            </Button>
            <Button variant="sm" onClick={() => handleCompare('lines')}>
              Compare by Lines
            </Button>
          </div>
        </div>
        <div className="primary-scrollbar flex flex-1 flex-col overflow-auto">
          <div className="flex flex-col justify-between gap-[10px] md:flex-row">
            <textarea
              value={text1}
              onChange={(e) => setText1(e.target.value)}
              placeholder="Enter first text"
              rows={10}
              cols={50}
              className="border border-red-500 p-[10px] text-gray-900 dark:bg-black dark:text-white"
            />
            <textarea
              value={text2}
              onChange={(e) => setText2(e.target.value)}
              placeholder="Enter second text"
              rows={10}
              cols={50}
              className="border border-green-500 p-[10px] text-gray-900 dark:bg-black dark:text-white"
            />
          </div>

          <div className="flex min-h-[300px] flex-1 flex-col overflow-hidden md:min-h-1">
            <h2>Diff Result</h2>
            {diffResult.length ? (
              <pre className="flex-1 overflow-auto overflow-y-scroll whitespace-pre-wrap border border-[#ccc] p-[10px]">
                {diffResult}
              </pre>
            ) : (
              'No difference found'
            )}
          </div>
        </div>
      </div>
    </Suspense>
  )
}

export default DiffCheckerPage
