import { HeadlessComponent } from '@/components/base.headless'
import { useState, useEffect } from 'react'

export interface IClockHeadless
  extends HeadlessComponent<{
    hours: number
    minutes: number
    seconds: number
    meridiem: string
  }> {}

const ClockHeadless: React.FC<IClockHeadless> = ({ children }: IClockHeadless) => {
  const [time, setTime] = useState({
    hours: 0,
    minutes: 0,
    seconds: 0,
    meridiem: '',
  })

  useEffect(() => {
    const interval = setInterval(() => {
      const date = new Date()
      setTime({
        hours: date.getHours(),
        minutes: date.getMinutes(),
        seconds: date.getSeconds(),
        meridiem: date.getHours() >= 12 ? 'PM' : 'AM',
      })
    }, 100)

    return () => clearInterval(interval)
  }, [])

  return children(time)
}

export default ClockHeadless
