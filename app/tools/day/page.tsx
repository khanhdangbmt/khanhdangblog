'use client'
import ClockHeadless from './_logic/Clock.headless.logic'

const ViewDate = () => {
  const today = new Date()
  const startOfYear = new Date(today.getFullYear(), 0, 1)
  const dayOfYear =
    Math.floor((today.getTime() - startOfYear.getTime()) / (1000 * 60 * 60 * 24)) + 1
  const isLeapYear = (year: number) => (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0
  const daysInYear = isLeapYear(today.getFullYear()) ? 366 : 365

  return (
    <div>
      <div className="flex items-center justify-center gap-4">
        <div className="w-fit text-[18px] font-semibold text-gray-800 dark:text-white">
          <div className="w-fit">D</div>
          <div className="w-fit">A</div>
          <div className="w-fit">Y</div>
        </div>
        <div className="flex gap-2 text-[64px] font-semibold md:text-[96px]">
          <div className="text-gray-800 dark:text-white">{dayOfYear}</div>/
          <div className="text-gray-400">{daysInYear}</div>
        </div>
      </div>
      <div className="text-center text-[16px]">
        The secret of getting ahead is{' '}
        <span className="font-semibold text-red-600">getting started</span>
      </div>
      <div className="flex items-center justify-center text-[20px] md:text-[24px]">
        <ClockHeadless>
          {({ hours, minutes, seconds, meridiem }) => {
            return (
              <div className="flex gap-2">
                <div>{today.toLocaleDateString('en-US', { weekday: 'long' })}</div>
                <div>{today.getDate()}</div>
                <div>{today.toLocaleString('en-US', { month: 'long' })}</div>
                <div>
                  {hours}:{minutes}:{seconds} {meridiem}
                </div>
              </div>
            )
          }}
        </ClockHeadless>
      </div>
    </div>
  )
}

export default ViewDate
