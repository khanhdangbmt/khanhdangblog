'use client'
import Button from '@/components/atoms/button'
import { Input } from '@/components/atoms/input'
import useDocumentTitle from 'hooks/useDocumentTitle'
import { Copy } from 'lucide-react'
import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid'

const UUIDGenerator = () => {
  const [uuids, setUUID] = useState<string[]>([])
  const [quantity, setQuantity] = useState<number>(1)

  useDocumentTitle('UUID Generator')

  return (
    <div>
      <h1 className="mb-4 text-2xl font-bold"> UUID generator</h1>
      <div className="flex justify-between gap-2">
        <div className='flex justify-center items-center gap-1'>
          Quantity:{' '}
          <Input type="number" value={quantity} onChange={(e) => setQuantity(+e.target.value)} />
        </div>
        <Button
          variant="sm"
          onClick={() => {
            const uuidArray = Array.from({ length: quantity }, () => uuidv4())
            setUUID(uuidArray)
          }}
        >
          Generate
        </Button>
      </div>
      <div className="mt-4">
        {uuids.map((uuid, index) => {
          return (
            <div key={uuid} className="flex items-center gap-2 hover:text-primary-500">
              {index + 1}. {uuid}{' '}
              <button
                onClick={() => navigator.clipboard.writeText(uuid)}
                className="inline-block cursor-pointer hover:text-primary-500"
                aria-label={`Copy UUID ${uuid}`}
              >
                <Copy size={18} />
              </button>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default UUIDGenerator
