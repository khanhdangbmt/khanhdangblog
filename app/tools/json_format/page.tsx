'use client'
import Button from '@/components/atoms/button'
import { useTheme } from 'next-themes'
import { Suspense, useState } from 'react'
import loadable from '@loadable/component'
import useDocumentTitle from 'hooks/useDocumentTitle'

const ReactJson = loadable(() => import('react-json-view'))

const JsonFormatPage = () => {
  const [isInvalid, setIsInvalid] = useState(false)
  const [json, setJson] = useState<string>('')
  const [isViewMode, setIsViewMode] = useState(false)
  const { theme } = useTheme()

  useDocumentTitle('JSON Formatter')

  const handleJsonChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setJson(e.target.value)
  }

  const handleFormat = () => {
    if (isInvalid) {
      setIsInvalid(false)
    }

    try {
      const formatted = JSON.stringify(eval('(' + json + ')'), null, 2)
      setJson(formatted)
    } catch (e) {
      setIsInvalid(true)
    }
  }

  const handleRemoveWhitespace = () => {
    try {
      const minified = JSON.stringify(eval('(' + json + ')'))
      setJson(minified)
    } catch (e) {
      setIsInvalid(true)
    }
  }

  const handleClear = () => {
    setJson('')
    setIsInvalid(false)
  }

  const toggleViewMode = () => {
    if (!isViewMode) {
      try {
        JSON.parse(json)
      } catch (e) {
        alert('Invalid JSON')
        return
      }
    }
    setIsViewMode(!isViewMode)
  }

  return (
    <Suspense fallback="loading...">
      <div className="flex h-full w-full flex-col">
        {/* Headers and toggle */}
        <div className="mb-4 flex items-center justify-between">
          <h1 className="text-2xl font-bold">JSON Formatter</h1>
          <Button onClick={toggleViewMode}>{isViewMode ? 'Edit' : 'View'}</Button>
        </div>
        {/* Tools */}
        {!isViewMode && (
          <div className="flex gap-2">
            <Button variant="sm" onClick={handleFormat} className="hover:text-primary-500">
              Format
            </Button>
            <Button
              variant="sm"
              onClick={handleRemoveWhitespace}
              className="hover:text-primary-500"
            >
              Remove Whitespace
            </Button>
            <Button variant="sm" onClick={handleClear} className="hover:text-primary-500">
              Clear
            </Button>
            <Button
              variant="sm"
              onClick={() => {
                navigator.clipboard.writeText(json)
              }}
              className="hover:text-primary-500"
            >
              Copy
            </Button>
            <Button
              variant="sm"
              onClick={async () => {
                const text = await navigator.clipboard.readText()
                setJson(text)
              }}
              className="hover:text-primary-500"
            >
              Paste
            </Button>
          </div>
        )}

        {/* View/Edit */}
        <div className="primary-scrollbar h-full flex-1 overflow-auto pt-2">
          {isViewMode ? (
            <ReactJson src={JSON.parse(json)} theme={theme === 'dark' ? 'tomorrow' : undefined} />
          ) : (
            <textarea
              value={json}
              onChange={handleJsonChange}
              placeholder="Enter JSON here"
              className={`primary-scrollbar h-full w-full text-gray-900 dark:bg-black dark:text-white ${isInvalid ? 'border-red-500' : ''}`}
              style={{ height: 'calc(100% - 50px)' }}
              rows={10}
              onKeyDown={(e) => {
                if (e.key === 'Tab') {
                  e.preventDefault()
                  const start = e.currentTarget.selectionStart
                  const end = e.currentTarget.selectionEnd
                  setJson(json.substring(0, start) + '\t' + json.substring(end))
                  setTimeout(() => {
                    e.currentTarget.selectionStart = e.currentTarget.selectionEnd = start + 1
                  }, 0)
                }
              }}
            />
          )}
        </div>
      </div>
    </Suspense>
  )
}

export default JsonFormatPage
