import Link from 'next/link'
import { useMemo } from 'react'

const ToolsPage = () => {
  const listTools = useMemo(
    () => [
      { name: 'JSON formatter', url: '/tools/json_format' },
      { name: 'Diff Checker', url: '/tools/diff_checker' },
      { name: 'Edit CSV', url: '/tools/edit_csv' },
      { name: 'Day of the year', url: '/tools/day' },
      { name: 'Text Count', url: '/tools/text_count' },
      { name: 'HTML Viewer', url: '/tools/html_viewer' },
      { name: 'Text Generator', url: '/tools/text_generator' },
      { name: 'UUID Generator', url: '/tools/uuid_generator' },
    ],
    []
  )

  return (
    <div>
      <ul>
        {listTools.map((tool) => {
          return (
            <li key={tool.url} style={{ listStyleType: 'dashed' }} className="cursor-pointer">
              <Link className="text-primary-500 hover:text-primary-600" href={tool.url}>
                {tool.name}
              </Link>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default ToolsPage
