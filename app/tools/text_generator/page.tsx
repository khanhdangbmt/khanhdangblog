'use client'
import Button from '@/components/atoms/button'
import useDocumentTitle from 'hooks/useDocumentTitle'
import { LoremIpsum } from 'lorem-ipsum'
import { useState } from 'react'

const TextGenerator = () => {
  const [result, setResult] = useState('')
  const [size, setSize] = useState<number>(10)
  const [textType, setTextType] = useState('paragraphs')
  useDocumentTitle('Text Generator')

  const lorem = new LoremIpsum({
    sentencesPerParagraph: {
      max: 8,
      min: 4,
    },
    wordsPerSentence: {
      max: 16,
      min: 4,
    },
  })

  return (
    <div>
      <h1 className="mb-4 text-2xl font-bold">Text generator</h1>
      <div>
        <div className="mb-2 flex justify-between gap-2">
          <div>
            Size: <input type="number" onChange={(e) => setSize(+e.target.value)} />
          </div>
          <Button
            onClick={() => {
              switch (textType) {
                case 'paragraphs':
                  setResult(lorem.generateParagraphs(size))
                  break
                case 'sentences':
                  setResult(lorem.generateSentences(size))
                  break
                case 'words':
                  setResult(lorem.generateWords(size))
                  break
                default:
                  setResult(lorem.generateParagraphs(size))
              }
            }}
          >
            Generate
          </Button>
        </div>
        <div className="mb-2 flex items-center gap-2">
          <input
            type="radio"
            id="paragraphs"
            name="textType"
            value="paragraphs"
            defaultChecked
            onChange={(e) => setTextType(e.target.value)}
          />
          <label htmlFor="paragraphs">Paragraphs</label>

          <input
            type="radio"
            id="sentences"
            name="textType"
            value="sentences"
            onChange={(e) => setTextType(e.target.value)}
          />
          <label htmlFor="sentences">Sentences</label>

          <input
            type="radio"
            id="words"
            name="textType"
            value="words"
            onChange={(e) => setTextType(e.target.value)}
          />
          <label htmlFor="words">Words</label>
        </div>
      </div>
      <div className="flex flex-col gap-2">
        <textarea name="result_lorem" id="result_lorem" value={result} rows={15}></textarea>
      </div>
    </div>
  )
}

export default TextGenerator
