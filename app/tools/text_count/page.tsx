'use client'

import { useState } from 'react'

const TextCount = () => {
  const [text, setText] = useState('')

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value)
  }
  console.log(text.split(' '))
  return (
    <div>
      <h1 className="text-2xl font-bold">Text count</h1>
      <textarea
        className="w-full p-[10px] text-gray-900 dark:bg-black dark:text-white"
        name="text-count"
        id="text-count"
        value={text}
        placeholder="Enter your text here"
        rows={10}
        onChange={onChange}
      ></textarea>
      <div className="flex w-full flex-row items-end justify-end gap-2">
        <div>{!text ? 0 : text.split(/\s+/).filter((item) => /\w+/.test(item)).length} words</div>
        <div>{text.split('').length} characters</div>
      </div>
    </div>
  )
}
export default TextCount
