'use client'

import useDocumentTitle from 'hooks/useDocumentTitle'
import { useState } from 'react'

const HTMLViewer = () => {
  const [html, setHtml] = useState<string>('')
  useDocumentTitle('HTML Viewer')

  return (
    <div>
      <h1 className="mb-4 text-2xl font-bold">HTML Editor</h1>
      <textarea
        name="html-input"
        id="html-input"
        onChange={(e) => setHtml(e.target.value)}
        placeholder="Enter your HTML code here"
        rows={10}
        cols={50}
        className="w-full p-[10px] text-gray-900 dark:bg-black dark:text-white"
      ></textarea>
      Preview:
      <div className="border border-gray-200 p-2">
        <iframe srcDoc={html} frameBorder="0" title="html-viewer-iframe"></iframe>
      </div>
    </div>
  )
}
export default HTMLViewer
