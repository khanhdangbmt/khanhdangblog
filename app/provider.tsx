'use client'

import { Toaster } from '@/components/atoms/toaster'

export function ThemeProviders({ children }: { readonly children: React.ReactNode }) {
  return (
    <>
      {children}
      <Toaster />
    </>
  )
}
