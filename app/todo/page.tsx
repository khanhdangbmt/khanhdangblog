'use client'

import { useState } from 'react'
import CreateTodo from './_ui/CreateTodo'

const TodoPage = () => {
  const [todos, setTodos] = useState<
    { id: number; title: string; isCompleted: boolean; createdAt: string }[]
  >([])

  const onCreateTodo = async (title) => {
    console.log('title', title)
    setTodos((prev) => [
      ...prev,
      { id: prev.length + 1, title, isCompleted: false, createdAt: new Date().toISOString() },
    ])
  }

  const markTodoAsCompleted = async (id, checked) => {
    console.log('join markTodoAsCompleted', id)
    setTodos((prev) =>
      prev.map((todo) => {
        if (todo.id === id) {
          return { ...todo, isCompleted: checked }
        }
        return todo
      })
    )
  }

  const onDeleteTodo = async (id) => {
    console.log('join onDeleteTodo', id)
    setTodos((prev) => prev.filter((todo) => todo.id !== id))
  }

  console.log(todos)

  const sortedTodos = todos
    .toSorted((a, b) => Number(a.isCompleted) - Number(b.isCompleted))
    .sort((a, b) => Number(b.createdAt) - Number(a.createdAt))

  return (
    <>
      <h1>Todo Page</h1>
      <CreateTodo onCreate={onCreateTodo} />

      <div>
        {todos.length > 0 ? (
          sortedTodos.map((todo) => (
            <div key={todo.id} className="flex items-center justify-between">
              <div>
                <input
                  onChange={(e) => markTodoAsCompleted(todo.id, e.target.checked)}
                  type="checkbox"
                  checked={todo.isCompleted}
                  className="mr-2 cursor-pointer"
                />
                <span className={todo.isCompleted ? 'italic line-through' : ''}>{todo.title}</span>
              </div>
              <button onClick={() => onDeleteTodo(todo.id)} className="text-red-500">
                x
              </button>
            </div>
          ))
        ) : (
          <p>No todos available</p>
        )}
      </div>
    </>
  )
}

export default TodoPage
