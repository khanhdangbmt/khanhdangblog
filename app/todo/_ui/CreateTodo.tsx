'use client'
import React, { useState } from 'react'

const CreateTodo = ({ onCreate }: { onCreate: (title) => void }) => {
  const [todo, setTodo] = useState('')

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    onCreate(todo)
    setTodo('')
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={todo}
          onChange={(e) => setTodo(e.target.value)}
          placeholder="Enter a new todo"
        />
      </form>
    </div>
  )
}

export default CreateTodo
