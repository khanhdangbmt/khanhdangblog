import { create } from 'zustand'
import { persist } from 'zustand/middleware'

type Note = {
  id: string
  title: string
  content: string
  createdAt: number
}

type NoteStore = {
  notes: Note[]
  selectedNoteId: string | null
  addNote: () => void
  updateNote: (id: string, content: string) => void
  deleteNote: (id: string) => void
  selectNote: (id: string) => void
}

export const useNoteStore = create<NoteStore>()(
  persist(
    (set) => ({
      notes: [],
      selectedNoteId: null,
      addNote: () =>
        set((state) => {
          const newNote = {
            id: crypto.randomUUID(),
            title: 'New Note',
            content: '',
            createdAt: Date.now(),
          }
          return { notes: [newNote, ...state.notes], selectedNoteId: newNote.id }
        }),
      updateNote: (id, content) =>
        set((state) => ({
          notes: state.notes.map((note) =>
            note.id === id
              ? { ...note, content, title: content.split('\n')[0] || 'Untitled' }
              : note
          ),
        })),
      deleteNote: (id) =>
        set((state) => ({
          notes: state.notes.filter((note) => note.id !== id),
          selectedNoteId: state.selectedNoteId === id ? null : state.selectedNoteId,
        })),
      selectNote: (id) => set(() => ({ selectedNoteId: id })),
    }),
    { name: 'notes-storage' }
  )
)
