import siteMetadata from '@/data/siteMetadata'
import headerNavLinks from '@/data/headerNavLinks'
import Link from '../atoms/Link'
import MobileNav from './MobileNav'
import ThemeSwitch from '../moleculers/ThemeSwitch'
// import SearchButton from './SearchButton'
import GradientButton from '../atoms/GradientButton'
import GoogleLogin from '../moleculers/GoogleLogin'

const Header = () => {
  return (
    <header className="flex items-center justify-between py-10">
      <div>
        <Link href="/" aria-label={siteMetadata.headerTitle}>
          <div className="flex items-center justify-between">
            {typeof siteMetadata.headerTitle === 'string' ? (
              <div className="hidden h-6 text-2xl font-semibold sm:block">
                {siteMetadata.headerTitle}
              </div>
            ) : (
              siteMetadata.headerTitle
            )}
          </div>
        </Link>
      </div>
      <div className="flex items-center space-x-4 leading-5 sm:space-x-6">
        {headerNavLinks
          .filter((link) => link.href !== '/')
          .map((link) => (
            <Link
              key={link.title}
              href={link.href}
              className="hidden cursor-pointer font-medium text-gray-900 dark:text-gray-100 sm:block"
            >
              {link.title.toLocaleLowerCase().includes('tool') ? (
                <GradientButton className="p-1">{link.title}</GradientButton>
              ) : (
                link.title
              )}
            </Link>
          ))}
        {/* <SearchButton /> */}
        <ThemeSwitch />
        <MobileNav />
        {<GoogleLogin />}
      </div>
    </header>
  )
}

export default Header
