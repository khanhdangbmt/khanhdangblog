'use client'
import { useEffect } from 'react'

const useEscPress = (handler: () => void) => {
  useEffect(() => {
    document.addEventListener('keydown', (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        handler()
      }
    })
    return () => {
      document.removeEventListener('keydown', handler)
    }
  }, [])
}

export default useEscPress
