'use client'
import {
  CalendarDays,
  FolderKanban,
  Home,
  MessageSquareText,
  Plus,
  User,
  Wrench,
} from 'lucide-react'
import 'css/circular-menu.css'
import { useEffect } from 'react'
import Link from 'next/link'
import useClickOutside from './useClickoutside'
import useEscPress from './useEscPress'

const CircularMenu = () => {
  const menuRef = useClickOutside(() => {
    const menu = document.querySelector('.menu')
    if (menu) {
      menu.classList.remove('active')
    }
  })

  useEscPress(() => {
    const menu = document.querySelector('.menu')
    if (menu) {
      menu.classList.remove('active')
    }
  })

  useEffect(() => {
    const toggle = document.querySelector('.toggle')
    const menu = document.querySelector('.menu')
    if (!toggle || !menu) {
      return
    } else {
      ;(toggle as HTMLElement).onclick = function () {
        menu.classList.toggle('active')
      }
    }
  }, [])

  return (
    <div className="circular-menu" ref={menuRef}>
      <div className="menu">
        <div className="toggle dark:color-gray-500 cursor-pointer">
          <Plus color="black" />
        </div>
        <li style={{ '--i': 0 } as React.CSSProperties}>
          <Link href={'/'}>
            <Home />
          </Link>
        </li>

        <li style={{ '--i': 1 } as React.CSSProperties}>
          <Link href={'/about'}>
            <User />
          </Link>
        </li>
        <li style={{ '--i': 2 } as React.CSSProperties}>
          <Link href={'/tools'}>
            <Wrench />
          </Link>
        </li>
        <li style={{ '--i': 3 } as React.CSSProperties}>
          <Link href={'/blog'}>
            <MessageSquareText />
          </Link>
        </li>
        <li style={{ '--i': 4 } as React.CSSProperties}>
          <Link href={'/projects'}>
            <FolderKanban />
          </Link>
        </li>
        <li style={{ '--i': 5 } as React.CSSProperties}>
          <Link href={'/tools/day'}>
            <CalendarDays />
          </Link>
        </li>
      </div>
    </div>
  )
}

export default CircularMenu
