'use client'
import { createClient } from 'libs/supabase/client'
import Button from '../atoms/button'
import { LogIn } from 'lucide-react'
import { useEffect, useState } from 'react'
import { User } from '@supabase/supabase-js'
import { Avatar, AvatarImage } from '../atoms/avatar'
import { AvatarFallback } from '@radix-ui/react-avatar'
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from '../atoms/dropdown-menu'
import { Skeleton } from '../atoms/skeleton'

const GoogleLogin = () => {
  const [user, setUser] = useState<User | null>(null)
  const [loading, setLoading] = useState(true)
  const supabase = createClient()

  const handleLogin = async () => {
    await supabase.auth.signInWithOAuth({
      provider: 'google',
      options: {
        redirectTo: process.env.NEXT_PUBLIC_GOOGLE_CALLBACK_URL,
      },
    })
  }

  const getUserInfo = async () => {
    const user = await supabase.auth.getSession()
    return user.data?.session?.user
  }

  useEffect(() => {
    getUserInfo()
      .then((data) => {
        data && setUser(data)
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])

  const handleLogout = async () => {
    await supabase.auth.signOut()
    setUser(null)
  }

  if (loading) return null

  if (!user)
    return (
      <Button onClick={handleLogin} className="!px-2 !py-1">
        <LogIn size={18} />
      </Button>
    )

  return (
    <div>
      <DropdownMenu>
        <DropdownMenuTrigger>
          <Avatar>
            <AvatarImage src={user.user_metadata.avatar_url} alt={user.email} />
            <AvatarFallback>{user.email?.slice(0, 2)}</AvatarFallback>
          </Avatar>
        </DropdownMenuTrigger>
        <DropdownMenuContent>
          <DropdownMenuLabel>My Account</DropdownMenuLabel>
          <DropdownMenuSeparator />
          <DropdownMenuItem onClick={handleLogout} className="cursor-pointer">
            Logout
          </DropdownMenuItem>
          <DropdownMenuItem
            onClick={() => {
              window.location.href = '/private'
            }}
            className="cursor-pointer"
          >
            Panel
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </div>
  )
}

export default GoogleLogin
