import { ReactNode } from 'react'

export interface HeadlessComponent<T> {
  children: (data: T) => ReactNode
}
