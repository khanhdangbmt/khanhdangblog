import React from 'react'

interface ButtonProps {
  onClick?: () => void
  children: React.ReactNode
  disabled?: boolean
  className?: string
  variant?: 'default' | 'sm' | 'xl'
  type?: 'button' | 'submit' | 'reset'
}

const Button: React.FC<ButtonProps> = ({
  onClick,
  children,
  disabled = false,
  className = '',
  variant = 'default',
  type = 'button',
}) => {
  const baseClasses =
    'rounded bg-gray-200 text-gray-800 transition-colors duration-300 hover:bg-primary-700 hover:text-white'
  const sizeClasses = {
    default: 'px-4 py-2',
    sm: 'px-2 py-1 text-sm',
    xl: 'px-6 py-3 text-xl',
  }

  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`${baseClasses} ${sizeClasses[variant]} ${className}`}
      type={type}
    >
      {children}
    </button>
  )
}

export default Button
