import clsx from 'clsx'
import 'css/gradient-button.css'
import { ReactNode } from 'react'

const GradientButton = ({ children, className }: { children: ReactNode; className?: string }) => {
  return <button className={clsx('gradient-btn', className)}>{children}</button>
}
export default GradientButton
